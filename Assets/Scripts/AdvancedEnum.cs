﻿using System;
using System.Collections.Generic;

namespace Scripts
{
    [Serializable]
    public abstract class AdvancedEnum<TBaseEnum, TAdvancedEnum> where TBaseEnum : Enum
    {
        public TBaseEnum BaseEnumValue { get; }
        public static Dictionary<TBaseEnum, TAdvancedEnum> AllValues { get; } = new Dictionary<TBaseEnum, TAdvancedEnum>();

        protected AdvancedEnum(TBaseEnum baseEnumValue)
        {
            BaseEnumValue = baseEnumValue;
            AllValues.Add(baseEnumValue, (TAdvancedEnum)Convert.ChangeType(this, typeof(TAdvancedEnum)));
        }

        public static TAdvancedEnum GetFromBaseEnumValue(TBaseEnum baseEnumValue)
        {
            return AllValues[baseEnumValue];
        }

        public static bool operator ==(AdvancedEnum<TBaseEnum, TAdvancedEnum> first, AdvancedEnum<TBaseEnum, TAdvancedEnum> second)
        {
            return first?.Equals(second) ?? ReferenceEquals(second, null);
        }

        public static bool operator !=(AdvancedEnum<TBaseEnum, TAdvancedEnum> first, AdvancedEnum<TBaseEnum, TAdvancedEnum> second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            var enumValue = obj as AdvancedEnum<TBaseEnum, TAdvancedEnum>;

            return enumValue != null && ((AdvancedEnum<TBaseEnum, TAdvancedEnum>)obj).BaseEnumValue.Equals(BaseEnumValue);
        }

        public override string ToString()
        {
            return BaseEnumValue.ToString();
        }

        public override int GetHashCode()
        {
            return BaseEnumValue.GetHashCode();
        }
    }
}